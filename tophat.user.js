// ==UserScript==
// @name         TopHat
// @namespace    Glibertarians
// @version      1.0
// @description  try to take over the world!
// @author       Lafe Long
// @include      *glibertarians.com/2*
// @grant        none
// ==/UserScript==

// Make youtube links red
var youtubeLinks = document.evaluate(".//a[contains(@href,'youtube.com') or contains(@href,'youtu.be')]",
		document.body, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
	for (var i = youtubeLinks.snapshotLength - 1; i >= 0; i--) {
		var ytl = youtubeLinks.snapshotItem(i);
        ytl.style.color = 'red';
    }

// Open external links in new tab.
function link_is_external(link_element) {
    return (link_element.host !== window.location.host);
}
var anchors = document.getElementsByTagName('a');
for (var i = 0; i < anchors.length; i++) {
    if (link_is_external(anchors[i])) {
        anchors[i].target = "_blank";
    }
}

// Unbold blockquotes
var link = window.document.createElement('link');
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = 'data:text/css,' +
                // Selectors start here
                'blockquote { font-weight:normal; }' +
                '';
document.getElementsByTagName("head")[0].appendChild(link);

// Create Preview Button
var previewButton = document.createElement('button');
	previewButton.appendChild(document.createTextNode('Preview Comment'));
	previewButton.setAttribute('id', 'previewButton');
    var cPreview = document.getElementById('comment');
	cPreview.parentNode.insertBefore(previewButton, cPreview);

// Create Preview div and set placeholder text
var commentPreviewText = document.createElement('div');
    commentPreviewText.innerHTML   = '<h3>Comment Preview</h3><div id="previewbox" style="border:1px solid #ddd;background-color:#FEFDC8;padding:15px;"><p>Comment Preview</p></div>';
	cPreview.parentNode.insertBefore(commentPreviewText, cPreview.nextSibling);

// Create preview based on user comment, display onClick
function commentPreview(){
	var cmntTemp = document.getElementById('comment').value;
// Add line breaks for preview
    var fval = cmntTemp.replace(/\n/g,"<br/>");
    document.getElementById('previewbox').innerHTML = fval;
		return false;
				}
		document.getElementById('previewButton').onclick = commentPreview;

